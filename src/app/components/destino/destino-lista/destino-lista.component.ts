import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Destinos } from '../../../model/destinos.model';
import { DestinosApiClient } from '../../../model/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from 'src/app/model/destinos-state.model';

@Component({
  selector: 'app-destino-lista',
  templateUrl: './destino-lista.component.html',
  styleUrls: ['./destino-lista.component.css'],
  providers: [ DestinosApiClient ]
})
export class DestinoListaComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<Destinos>;
  updates:string[];
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000);
  });
  all;

  constructor(public destinosApiClient:DestinosApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();   
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(data => {
      const d = data;
      if (d != null) {
        this.updates.push('Se ha elegido a ' + d.title);
      }
    });
    store.select (state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() { }

  agregado(d: Destinos) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    // this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e: Destinos) {
    this.destinosApiClient.elegir(e);
    // this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll() { }

}