import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Destinos } from '../../../model/destinos.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { VoteUpAction, VoteDownAction } from '../../../model/destinos-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-card',
  templateUrl: './destino-card.component.html',
  styleUrls: ['./destino-card.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({ backgroundColor: 'PaleTurquoise' })),
      state('estadoNoFavorito', style({ backgroundColor: 'WhiteSmoke' })),
      transition('estadoNoFavorito => estadoFavorito', [ animate('0.5s') ]),
      transition('estadoFavorito => estadoNoFavorito', [ animate('0s') ]),
    ])
  ]
})

export class DestinoCardComponent implements OnInit {
  @Input() destino: Destinos;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-3 col-sm-6 d-flex flex-wrap producto'
  @Output() clicked: EventEmitter<Destinos>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {}

  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}