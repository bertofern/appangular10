import { Component, OnInit, Output, EventEmitter, forwardRef, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { Destinos } from '../../../model/destinos.model';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino',
  templateUrl: './form-destino.component.html',
  styleUrls: ['./form-destino.component.css']
})
export class FormDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Destinos>; 
  fg: FormGroup;
  minLong= 3;
  searchResults: string[];

  constructor(fg: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.fg = fg.group({
      title: ['', Validators.compose([
        Validators.required,
        this.titleValidator,
        this.titleValidatorParametrizable(this.minLong)
      ])],
      imageUrl: ['']
    });
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambio en el formulario: ', form)
    });
  }

  ngOnInit() {
    const elemTitle = <HTMLInputElement>document.getElementById('title');
    fromEvent(elemTitle, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 0),
      debounceTime(120),
      distinctUntilChanged(),
      switchMap(() => ajax('../../../assets/datos.json'))
    ).subscribe(AjaxResponse => {
      // console.log(AjaxResponse);
      // console.log(AjaxResponse.response);
      // this.searchResults = AjaxResponse.response;
      this.searchResults = AjaxResponse.response.filter(function(x){
        return x.toLowerCase().includes(elemTitle.value.toLowerCase());
      });
    });
  }

  guardar(title:string, imageUrl:string): boolean {
    const d = new Destinos(title, imageUrl);
    this.onItemAdded.emit(d);
    return false;
  }

  titleValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return { invalidTitle: true }
    }
    return null;
  }

  titleValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < this.minLong) {
        return { nimLongTitle: true }
      }
      return null;
    }
  }

}
