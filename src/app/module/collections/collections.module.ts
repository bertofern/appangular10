import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollectionsRoutingModule } from './collections-routing.module';
import { CollectionsHomeComponent } from './collections-home/collections-home.component';

// import { BrowserModule } from '@angular/platform-browser';
// import { HttpClientModule } from '@angular/common/http';

// import { SearchBarComponent } from '../../components/search-bar/search-bar.component';
// import { PageListComponent } from '../../components/page-list/page-list.component';

@NgModule({
  declarations: [
    CollectionsHomeComponent, 
    //SearchBarComponent, PageListComponent
  ],
  imports: [
    CommonModule,
    //BrowserModule,
    //HttpClientModule,
    CollectionsRoutingModule
  ],
  exports: [ ]
})
export class CollectionsModule { }
