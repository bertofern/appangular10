import { Injectable } from '@angular/core';
import { Destinos } from './destinos.model';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-state.model';
import { AppState } from './../app.module';

@Injectable({
  providedIn: 'root'
})

export class DestinosApiClient {
  destinos: Destinos[] = [];
  constructor(private store: Store<AppState>) {
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Barcelona', 'assets/barcelona.png', 2)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Amsterdam', 'assets/amsterdam.png', 5)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Berlín', 'assets/berlin.png', 7)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Londres', 'assets/londres.png', 4)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Poznan', 'assets/poznan.png', 3)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Praga', 'assets/praga.png', 4)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Zurich', 'assets/zurich.png', 1)));
    }

  add(d: Destinos) {
    this.store.dispatch(new NuevoDestinoAction(d));
  }
  
  elegir(d: Destinos) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  getById(id: String): Destinos {
    return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
  }

}
