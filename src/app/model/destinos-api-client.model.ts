import { forwardRef, Inject, Injectable } from '@angular/core';
import { Destinos } from './destinos.model';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-state.model';
import { AppConfig, AppState, APP_CONFIG, db } from './../app.module';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';


@Injectable()
export class DestinosApiClient {
  destinos: Destinos[] = [];
  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ) {
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Barcelona', 'assets/barcelona.png', 2)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Amsterdam', 'assets/amsterdam.png', 5)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Berlín', 'assets/berlin.png', 7)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Londres', 'assets/londres.png', 4)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Poznan', 'assets/poznan.png', 3)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Praga', 'assets/praga.png', 4)));
    this.store.dispatch(new NuevoDestinoAction(new Destinos('Zurich', 'assets/zurich.png', 1)));
  
    this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log('all store');
        console.log(data);
      });
      
  }

  add(d: Destinos) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.title }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('Todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos))
      }
    });
  }
  
  elegir(d: Destinos) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  getById(id: String): Destinos {
    return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
  }

}
