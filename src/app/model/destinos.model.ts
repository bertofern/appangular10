import {v4 as uuid} from 'uuid';

export class Destinos {
    private selected: boolean;
    public servicios: string[];
    id = uuid();
    constructor(public title:string, public imageUrl:string, public votes: number = 0) {
        this.servicios = ['WC', 'Desayuno'];
    }
    
    setSelected(s: boolean) {
      this.selected = s;
    }
    isSelected(): boolean {
      return this.selected;
    }
    voteUp(): any {
      this.votes++;
    }
    voteDown(): any {
      this.votes--;
    }
}