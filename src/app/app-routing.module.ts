import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DestinoDetalleComponent } from './components/destino/destino-detalle/destino-detalle.component';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login/login.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';

import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponent },
  { path: 'mas-info', component: VuelosMasInfoComponent },
  { path: ':id', component: VuelosDetalleComponent },
];

const routes: Routes = [
  { path: '', redirectTo:'home', pathMatch:'full' },
  { path: 'home', component: HomeComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'contact', loadChildren: () => import('./module/contact/contact.module').then(m =>m.ContactModule) },
  { path: 'services', loadChildren: () => import('./module/services/services.module').then(m =>m.ServicesModule) },
  { path: 'login', component: LoginComponent },
  { path: 'protected', component: ProtectedComponent, canActivate: [ UsuarioLogueadoGuard ] },
  { path: 'vuelos', component: VuelosComponent, canActivate: [ UsuarioLogueadoGuard ], children: childrenRoutesVuelos },
  //put at the end
  { path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
