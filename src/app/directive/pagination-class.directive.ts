import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appPaginationClass]'
})
export class PaginationClassDirective {
  constructor(private element: ElementRef) {}

  @Input('appPaginationClass') set classNames(classObj: any) {
    for (let key in classObj) {
      if (classObj[key]) {
        this.element.nativeElement.classList.add(key);
      } else {
        this.element.nativeElement.classList.remove(key);
      }
    }
  }
}
