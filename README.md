
* About me: [bertofern](https://bertofern.wordpress.com/)

### Desarrollo de páginas con Angular para el Curso "Full Stack Web Development" - Universidad Austral - coursera.org 

## Instación y Uso

```
$ npm install
```

**Para arrancar el servidor de Angular:**

```
$ ng serve
```
_Abrir en el navegador http://localhost:4200/_

**Para arrancar el servidor de la API con Express:**

```
$ cd express-server

$ npm install

$ node app.js
```
_Se interactuará con la API en: http://localhost:3000/_

---

## Testing

**Jasmine js para pruebas unitarias:**

```
$ ng test
```

**Cypress js para pruebas funcionales:**

```
$ npm run e2e
```
_Ejecutar: main-view.spec.ts_

---

## Acerca del proyecto

*Prácticas del Módulo 1:*

* Utilizar el ecosistema básico de Angular y el concepto de componentes
* Desarrollar una primera SPA básica con estilos de Bootstrap
* Utilizar el lenguaje Typescript

*Puntos:*

1. El paquete npm de Bootstrap instalado.
2. La importación de los css de bootstrap en los estilos globales de la app angular.
3. El desarrollo de al menos un componente donde se utilicen los estilos de Bootstrap
4. Al menos en un componente, la sintaxis {{ }} para cargar al menos un dato de alguna variable Typescript, por ejemplo, nombre o descripción.
5. Un componente contenedor de un listado de objetos, en el cual se vea un array de elementos, y éstos deben verse en el navegador.
6. En la plantilla html del listado, un tag UL con elementos LI, y con uso de la directiva ngfor, que esté siendo usada para iterar sobre los elementos a mostrar.
7. El uso de @HostBinding en al menos un componente
8. En el componente de listado, un formulario html con resoluciones de angular (es decir las variables de plantilla con #), que sea utilizado para ingresar los datos
9. Que con la sintaxis de variables de formulario con el token #, al hacer click en un botón de submit, se invoque a una función del componente en typescript, con el fin de agregar un nuevo ítem al listado
10. En el componente contenedor del listado, que en typescript exista la función en la cual se agregue al array de elementos a mostrar al nuevo elemento, y que, como consecuencia, y de manera reactiva, se actualice la interfaz de usuario.

---

*Prácticas del Módulo 2:*

* Implementar arquitecturas basadas en componentes.
* Crear formularios interactivos.
* Crear aplicaciones reactivas utilizando Redux.

*Puntos:*

1. Debe verse en un componente, el uso de un EventEmitter que sea decorado con @Output.
2. En el module.ts debe verse una configuración de rutas, con un path que emplee un redirect, y otro path que indique un component.
3. Debe verse en app.component.html que se usa un router outlet como enrutador raíz.
4. Debe verse en el código typescript de un componente, que en el constructor a partir de un formBuilder se configura el formGroup, donde se utilizan al menos 2 campos/controles.
5. Debe verse que se vinculan los controles del formGroup a elementos “input type text” en el html.
6. Debe verse en el código html de un componente, que contenga a otro componente que tenga un formulario, que en el componente padre existe un atributo de tipo Output donde se "reciba" el evento emitido por el componente hijo.
7. Debe existir un componente que contenga un formulario con al menos 2 campos, que al menos uno de ellos además de tener una validación de requerido, tenga una validación personalizada y parametrizable.
8. Debe verse en el html que se usa ngif y el método hasError, para mostrar mensajes de errores según corresponda para dichas validaciones.
9. Debe existir al menos un reducer con 2 actions para manejar el agregado y borrado de elementos en una colección.
10. Debe verse, implementada con redux, una funcionalidad de voto a favor y voto negativo sobre elementos de un listado, donde cada elemento tiene su propio contador de votos.

---

*Prácticas del Módulo 3:*

* Organizar en una aplicación los diferentes flujos de navegación.
* Desarrollar una aplicación modular.
* Integrar con un backend sin persistencia.

*Puntos:*

1. Un Guard que verifique, usando un servicio, si un usuario está logueado o no.
2. Un componente que sea protegido por un Guard cuando se configura su ruta.
3. Una variable de configuración u otro literal, que es inyectado como dependencia con un InjectionToken propio.
4. Una inyección de dependencias configurada con un useClass para vincular, por ejemplo, una interfaz a una clase concreta.
5. Una inyección de dependencias con un useExisting, que inyecte una clase X con una clase Y, que debe ser compatible, por ejemplo, porque Y hereda de X o, por ejemplo, porque X e Y heredan de Z.
6. Una aplicación express, que oficie de una API simple, que maneje un array en memoria de elementos y que debe retornar el array de elemento con un get y también debe permitir agregar elementos recibiendo JSON en un post.
7. Un servicio en angular que se comunique asíncronamente con el api usando Http.
8. Que el servicio que se comunica con el API, cuando asíncronamente el API retorne una respuesta afirmativa ante el agregado de un elemento, el servicio creado debe notificar a Redux con un Action. (Recordar, ese Action debe ser usado al menos para que se agregue ese nuevo elemento al estado de Redux, y por ende, repercuta en la interfaz de usuario).
9. Una base de datos Dexie con al menos una entidad y que sea inyectada a un componente o servicio.
10. Que el servicio que se comunica con el API, cuando se agrega un elemento de manera exitosa en el api, debe agregarse asíncronamente también en Dexie.

---

*Prácticas del Módulo 4:*

* Registrar sucesos de interés durante el ciclo de vida de los componentes Angular.
* Probar automáticamente el front-end de nuestra aplicación.
* Probar automáticamente los diferentes componentes de nuestra aplicación Angular.

*Puntos:*

1. Deben estar instalados los paquetes de npm para mapas de mapbox: tanto ngx-mapbox-gl como @types/mapbox-gl@0.49.0? (Se verifica con el comando "npm ls ngx-mapbox-gl").
2. Debe existir un marker en el mapa que, al hacerle click, se muestre un mensaje o popover.
3. Debe existir un componente que tenga al menos una animación cuando alguna propiedad cambie en dicho componente.
4. Debe verse una directiva personalizada para trackear/seguir/registrar la actividad de clicks del usuario.
5. Debe verse que la directiva reciba, por inyección de dependencias, la referencia al ElementRef, y debe suscribirse a los eventos del DOM para el click.
6. Debe existir en uno o más componentes, en sus templates html, el uso de la directiva personalizada, y que dicha directiva lea unos “tracking tags” del elemento del DOM, como se realizó en los tutoriales de video del curso.
7. Debe verse que, usando redux, se actualice la cuenta de "tracking tags", y dichos contadores, deben verse por pantalla y deben actualizarse de manera reactiva.
8. Debe existir un spec con Jasmine, donde todos los reducers usados en redux se deben probar con testing unitario (recordar que los reductores son funciones puras, es decir que deben actuar solamente sobre el estado y acción que reciben como parámetro, y deben retornar un nuevo estado, mutando el original).
9. Debe estar presente el paquete de cypress con npm y, además, deben existir al menos 3 tests de cypress propios, que prueben la aplicación angular.
10. Debe verse la integración con circleci (el archivo de configuración ".circleci/config.yml")